import sys

def metric_conversion(number, prevValue, newValue):
    if prevValue == 'mil':
        if newValue == 'mil':
            return number
        if newValue == 'cen':
            return number/10
        if newValue == 'dec':
            return number/100
        if newValue == 'met':
            return number/1000
        if newValue == 'kil':
            return number/1000000

    if prevValue == 'cen':
        if newValue == 'mil':
            return number*10
        if newValue == 'cen':
            return number
        if newValue == 'dec':
            return number/10
        if newValue == 'met':
            return number/100
        if newValue == 'kil':
            return number/100000

    if prevValue == 'dec':
        if newValue == 'mil':
            return number*100
        if newValue == 'cen':
            return number*10
        if newValue == 'dec':
            return number
        if newValue == 'met':
            return number/10
        if newValue == 'kil':
            return number/10000

    if prevValue == 'met':
        if newValue == 'mil':
            return number*1000
        if newValue == 'cen':
            return number*100
        if newValue == 'dec':
            return number*10
        if newValue == 'met':
            return number
        if newValue == 'kil':
            return number/1000

    if prevValue == 'kil':
        if newValue == 'mil':
            return number*1000000
        if newValue == 'cen':
            return number*100000
        if newValue == 'dec':
            return number*10000
        if newValue == 'met':
            return number*1000
        if newValue == 'kil':
            return number

if(len(sys.argv) > 2):
    number = int(sys.argv[1])
    prevValue = str(sys.argv[2])
    newValue = str(sys.argv[3])

    print(metric_conversion(number, prevValue, newValue))
