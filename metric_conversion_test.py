from metric_conversion import metric_conversion

def test_to_mil():
    assert metric_conversion(5, 'cen', 'mil') == 50
    assert metric_conversion(5, 'met', 'cen') == 500
    assert metric_conversion(5, 'mil', 'dec') == 0.05
    assert metric_conversion(5, 'kil', 'met') == 5000
    assert metric_conversion(5, 'mil', 'kil') == 0.000005